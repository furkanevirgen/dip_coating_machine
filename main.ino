/*     
        Dip Coating Machine
        
  @author     Furkan Evirgen
  @company    Bahcesehir University
  @location   Istanbul - 2014
   
   Peripheral Units;
  * LCD            6 Pin
  * Keypad         7 Pin
  * 3 Step Motor   6 Pin
  * 3 Sensor       3 Pin 
*/

#include <LiquidCrystal.h>
#include <Keypad.h>
#define MOTOR 50
#define DIR 52
#define UP false
#define DN true
#define STEPS 800
#define DW digitalWrite

typedef enum
{
  DIPPING,
  WAITING,
  HEATING,
  TOTAL
} modes;

typedef enum
{
  GLASS,
  SOLUTION,
  PULL,
  ALL
} messages;

String txt[TOTAL] = {"DIPPING","WAITING","HEATING"};
String msg[ALL] = {"->Glass","->Solution","->Pull"};
unsigned mv[TOTAL] = {7,0,0};
// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(32,30,28,26,24,22);
// initialize the keypad
const byte ROWS = 4; //four rows
const byte COLS = 3; //three columns
char keys[ROWS][COLS] = {
    {'1','2','3'},
    {'4','5','6'},
    {'7','8','9'},
    {'*','0','#'}
};

byte rowPins[ROWS] = {21,20,19,18}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {17,16,15};      //connect to the column pinouts of the keypad

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

/////////////////////////// Variables /////////////////////////////////
byte reset = A0;
byte led = 13;
byte motor1[]={50,48};  //motor pins
byte motor2[]={40,42};  //motor pins
byte motor3[]={41,43}; //motor Pins
byte h_sensor=51;
byte w_sensor=47;
byte d_sensor=49;
int heat,wait,dip,repeat,wrap;
/////////////////////Function Prototypes//////////////////////////////
void main_menu();//main screen of the program
void manual();  // controlling motors manually
unsigned int setting(String state);//settings of the parameters
void start(boolean s);
void startWire();
void stepper();
unsigned int calc(char d1, char d2, char d3);
//void indicator(String label, int x);
/////////////////////// Setup Function ///////////////////////////////
void setup() {
  // set up the LCD's number of columns and rows: 
  lcd.begin(20, 4);
  pinMode(led, OUTPUT);
  pinMode(MOTOR,true);
  pinMode(DIR,true);
  lcd.clear();
  chdir(DN);
  resetpos();
}
//////////////////////// Main Function //////////////////////////////
void loop(){
main_menu();
while(1){
if (keypad.getKey() == '1'){
    heat=setting("HEATING DURATION");
    wait=setting("WAITING DURATION");
    dip=setting("DIPPING DURATION");
    repeat=setting("REPETITION");
    start(0);  
   }
   
else if (keypad.getKey()=='2'){
  wrap=setting("WRAPPING DURATION");

  start(1);
}

else if (keypad.getKey()=='3'){
  manual();
}}}

///////////////////////////// Functions //////////////////////////////

unsigned int setting(String state){
    char d1,d2,d3;
    unsigned int value;
    lcd.clear();
    if(state=="REPETITION"){
      lcd.setCursor(5,0);
      lcd.print(state);
      lcd.setCursor(10,2);
      lcd.print("Times");
      lcd.setCursor(6,2);
    }  
    else{
      lcd.setCursor(2,0);
      lcd.print(state);
      lcd.setCursor(10,2);
      lcd.print("Sec");
      lcd.setCursor(6,2);
    }
    while(1){
    lcd.blink();
   d1=keypad.getKey(); delay(50);
   delay(50);
  if(d1){
    lcd.print(d1);
    while(1){
      lcd.setCursor(7,2);
      d2=keypad.getKey(); delay(50);
      if(d2){
        lcd.print(d2);
    while(1){
      lcd.setCursor(8,2);   
      d3=keypad.getKey(); delay(50);
      if(d3){
        lcd.print(d3);
        while(1){
          lcd.noBlink();
          if(keypad.getKey()=='#'){       
              value=calc(d1,d2,d3);
                return value;}
          else if (keypad.getKey()=='*'){
                 asm("    jmp 0");
              }}}}}}}}}

  
void start(boolean s){
  lcd.clear();
  while (1){
  if (s==0){
  lcd.setCursor(2,1);
    lcd.print("H=");
    lcd.print(heat);
     lcd.setCursor(12,1);
     lcd.print("W=");
    lcd.print(wait);
     lcd.setCursor(2,2);
     lcd.print("D=");
    lcd.print(dip);
     lcd.setCursor(12,2);
     lcd.print("R=");
     lcd.print(repeat);
  }
  else if (s==1){
  lcd.setCursor(1,1);
    lcd.print("Duration = ");
    lcd.print(wrap);
    lcd.print(" Sec");
   }
     lcd.setCursor(0,0);
     lcd.print("#######START########");
     lcd.setCursor(0,3);
     lcd.print("*******RESET********");
     while(1){
     if(keypad.getKey()=='#'){      
        run();
     }
     else if(keypad.getKey()=='*'){
       digitalWrite(reset,LOW);
       asm("    jmp 0");
     }}}}
 
 void main_menu(){
  lcd.setCursor(4,0);
  lcd.print(" MAIN MENU");
  lcd.setCursor(0,1);
  lcd.print("1 - GLASS COATING");
  lcd.setCursor(0,2);
  lcd.print("2 - WIRE  COATING");
  lcd.setCursor(0,3);
  lcd.print("3 - MANUAL MODE");
 }
unsigned int calc(char d1, char d2, char d3){
  unsigned int id1,id2,id3;
  id1=((int)d1-48)*100;
  id2=((int)d2-48)*10;
  id3=((int)d3-48);
  return id1+id2+id3;
}


void stepper(boolean dir, byte motor[], int stp){
  unsigned int i=0;
  if (dir==0){
    digitalWrite(motor[0], LOW);
  }
  else {
    digitalWrite(motor[0], HIGH);
  }
  
  while(i<stp){
    digitalWrite(motor[1],HIGH);
    delay(1);
    digitalWrite(motor[1],LOW);
    delay(1);
  }
}

void manual(){
  lcd.clear();
  lcd.setCursor(4,0);
  lcd.print("MANUAL MODE");
  lcd.setCursor(0,1);
  lcd.print("1-CW  MOTOR 1  CCW-3");
  lcd.setCursor(0,2);
  lcd.print("4-CW  MOTOR 2  CCW-6");
  lcd.setCursor(0,3);
  lcd.print("7-CW  MOTOR 3  CCW-9");
  while(1){
  if(keypad.getKey()=='*'){
       asm("    jmp 0");

}}}


void smove(short steps)
{
  for(int i=0;i<steps*STEPS;i++)
  { 
    DW(MOTOR,true);
    delay(1);
    DW(MOTOR,false);
    delay(1);
  }
  return;
}

void chdir(boolean newdir)
{
  DW(DIR,newdir);
  return;
}

unsigned long await[TOTAL];
void run()
{
  lcd.clear();
  await[DIPPING] = dip;
  await[HEATING] = heat;
  await[WAITING] = wait;
  // input wait
  if(repeat <= 0) repeat = 1;
  chdir(UP);
  smove(7);
  while(!waittill(SOLUTION,'#'));
  for(int i=0;i<repeat;i++)
  { //edit
    resetpos();
    indic(DIPPING,true);
    chdir(UP);
    smove(38);
    indic(HEATING,true);
    chdir(DN);
    smove(33);
    indic(WAITING,true);
    if(i+1 == repeat)
    {
      while(!waittill(SOLUTION,'#'));
      resetpos();
    }
  }
  while(!waittill(PULL,'#'));
  resetpos();
  return;
}

boolean waittill(unsigned message, char key)
{
  lcd.clear();
  lcd.setCursor(0,0); //edit
  lcd.print(msg[message]);
  if(keypad.getKey() == key)
  {
    delay(50);
    return true;
  }
  delay(50);
  return false;
}

void resetpos()
{
  chdir(DN);
  while(!digitalRead(49)) //look
  {
    DW(MOTOR,true);
    delay(1);
    DW(MOTOR,false);
    delay(1);
  }
  return;
}

void indic(unsigned mode, boolean counter)
{
  lcd.clear();
  lcd.setCursor(0,0); //edit
  lcd.print(txt[mode]);
  for(int i=await[mode];i>0;i--)
  {
    if(counter)
    {
      lcd.setCursor(20-3,0); //edit
      lcd.print("   ");
      if(i<100)
        lcd.print(0);
      if(i<10)
        lcd.print(0);
      lcd.print(i);
    }
    delay(1000);
  }
  return;
}